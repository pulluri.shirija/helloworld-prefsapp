/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <clutter/clutter.h>

#include "helloworld-prefs-app.h"

int
main (int argc, char **argv)
{
  g_autoptr (GApplication) app = NULL;

  if (clutter_init (0, NULL) != CLUTTER_INIT_SUCCESS)
    {
      g_error ("Failed to initialize Clutter");
      return -1;
    }

  app = G_APPLICATION (g_object_new (HLW_TYPE_PREFS_APP,
      "application-id", BUNDLE_ID,
      "flags", G_APPLICATION_HANDLES_OPEN,
      NULL));

  return g_application_run (app, argc, argv);
}

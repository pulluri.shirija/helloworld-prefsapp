/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "helloworld-prefs-app.h"

#include <mildenhall/mildenhall.h>

struct _HlwPrefsApp
{
  GApplication parent;

  ClutterActor *stage;                  /* owned */

  MildenhallButtonSpeller *button;      /* unowned */

  GSettings *settings;                  /* owned */
};

G_DEFINE_TYPE (HlwPrefsApp, hlw_prefs_app, G_TYPE_APPLICATION);

enum
{
  COLUMN_TEXT = 0,
  COLUMN_LAST = COLUMN_TEXT,
  N_COLUMN
};

static void
button_pressed_cb (MildenhallButtonSpeller *button,
                   gpointer user_data)
{
  const ClutterColor *bg_color;
  const gchar *to_be_color_name;
  g_autofree gchar *last_color_name = NULL;

  HlwPrefsApp *self = HLW_PREFS_APP (user_data);

  g_message ("Clicked Center button");

  last_color_name = g_settings_get_string (self->settings, "color-name");

  /* For the reason of simplicity, the button action switches the background
   * color between "Green" and "Blue".
   * Once changed the background color, the value of "color-name" in settings
   * will be updated. */
  if (g_strcmp0 (last_color_name, "Green") == 0)
    {
      to_be_color_name = "Blue";
      bg_color = CLUTTER_COLOR_DarkBlue;
    }
  else
    {
      /* Assume that the color-name was "Blue" */
      to_be_color_name = "Green";
      bg_color = CLUTTER_COLOR_DarkGreen;
    }

  /* Change screen background color */
  clutter_actor_set_background_color (self->stage, bg_color);

  if (!g_settings_set_string (self->settings, "color-name", to_be_color_name))
    {
      g_message ("The key, color-name, isn't writable.");
    }

  g_settings_sync ();

  v_button_speller_set_text (CLUTTER_ACTOR (self->button), to_be_color_name);
}

static gboolean
on_stage_destroy (ClutterActor *stage,
                  gpointer user_data)
{
  GApplication *app = user_data;

  g_debug ("The main stage is destroyed.");

  /* chain up */
  CLUTTER_ACTOR_GET_CLASS (stage)->destroy (stage);

  /* The main window is detroyed (closed) so the application
   * needs to be released */
  g_application_release (app);

  return TRUE;
}


static void
startup (GApplication *app)
{
  g_autoptr (GObject) widget_object = NULL;
  g_autoptr (ThornburyItemFactory) item_factory = NULL;
  g_autofree gchar *widget_properties_file = NULL;
  g_autofree gchar *last_color_name = NULL;
  ThornburyModel *model;

  HlwPrefsApp *self = HLW_PREFS_APP (app);

  g_application_hold (app);

  /* chain up */
  G_APPLICATION_CLASS (hlw_prefs_app_parent_class)->startup (app);

  /* Create GSettings instance */
  self->settings = g_settings_new (g_application_get_application_id (app));

  /* build ui components */
  self->stage = mildenhall_stage_new (g_application_get_application_id (app));

  g_signal_connect (G_OBJECT (self->stage),
      "destroy", G_CALLBACK (on_stage_destroy),
      self);

  widget_properties_file = g_build_filename (PKGDATADIR,
                                             "button_speller_text_prop.json",
                                             NULL);

  item_factory = thornbury_item_factory_generate_widget_with_props (
      MILDENHALL_TYPE_BUTTON_SPELLER,
      widget_properties_file);

  g_object_get (item_factory,
                "object", &widget_object,
                NULL);

  self->button = MILDENHALL_BUTTON_SPELLER (widget_object);

  model = THORNBURY_MODEL (
      thornbury_list_model_new (N_COLUMN,
                                G_TYPE_STRING,
                                NULL,
                                -1));

  thornbury_model_append (model, 
                          COLUMN_TEXT, "",
                          -1);

  g_object_set (self->button,
                "model", model,
                NULL);

  g_object_unref (model);

  clutter_actor_add_child (self->stage, CLUTTER_ACTOR (self->button));

  /* Set background color from preference data */
  last_color_name = g_settings_get_string (self->settings, "color-name");

  v_button_speller_set_text (CLUTTER_ACTOR (self->button), last_color_name);

  g_message ("color-name from GSettings: '%s'.", last_color_name);

  if (g_strcmp0 (last_color_name, "Green") == 0)
    {
      clutter_actor_set_background_color (self->stage, CLUTTER_COLOR_DarkGreen);
    }
  else
    {
      clutter_actor_set_background_color (self->stage, CLUTTER_COLOR_DarkBlue);
    }

  g_signal_connect (self->button,
      "button-press", G_CALLBACK (button_pressed_cb),
      self);
}

static void
activate (GApplication *app)
{
  HlwPrefsApp *self = HLW_PREFS_APP (app);

  clutter_actor_show (self->stage);
}

static void
hlw_prefs_app_dispose (GObject *object)
{
  HlwPrefsApp *self = HLW_PREFS_APP (object);

  g_clear_object (&self->stage);
  g_clear_object (&self->settings);

  G_OBJECT_CLASS (hlw_prefs_app_parent_class)->dispose (object);
}

static void
hlw_prefs_app_class_init (HlwPrefsAppClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->dispose = hlw_prefs_app_dispose;

  app_class->startup = startup;
  app_class->activate = activate;
}

static void
hlw_prefs_app_init (HlwPrefsApp *self)
{
}
